//
//  ViewController.swift
//  push
//
//  Created by Cleber Cassiani on 29/9/18.
//  Copyright © 2018 Cleber Cassiani. All rights reserved.
//

import UIKit


/// ViewController is the Homepage of the app
class ViewController: UIViewController {
    
    /// User name text field
    @IBOutlet weak var userName: UITextField!
    
    /// Program name text label
    @IBOutlet weak var streakLabel: UILabel!
    
    /// Helper variable to pass on the name ojn the screen flow
    var nameTyped : String? = "Type your name here"
    
    /// Loads the view
    override func viewDidLoad() {
        super.viewDidLoad()
        userName.text = nameTyped
    }
    
    /// This button takes the user to the PUSH exercises page
    ///
    /// - Parameter sender: self
    @IBAction func pushPressed(_ sender: Any) {
        performSegue(withIdentifier: "goToSecondScreen", sender: self)
    }
    
    /// This button takes the user to the TASK exercises page
    ///
    /// - Parameter sender: self
    @IBAction func taskPressed(_ sender: Any) {
        performSegue(withIdentifier: "goToTask", sender: self)
    }
    /// This button takes the user to the program information page
    ///
    /// - Parameter sender: self
    @IBAction func infoPressed(_ sender: Any) {
        performSegue(withIdentifier: "goToInfo", sender: self)
    }
    
    /// This method helps to pass on data to the next screen. It is used to display the user name on the next screens and help with the email class.
    ///
    /// - Parameters:
    ///   - segue: the next view
    ///   - sender: self
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToSecondScreen" {
            let destinationVC = segue.destination as! PushViewController
            destinationVC.nameTyped = userName.text
        }
        if segue.identifier == "goToInfo" {
            let destinationVC = segue.destination as! InfoViewController
            destinationVC.nameTyped = userName.text
        }
        if segue.identifier == "goToDisclaimer" {
            let destinationVC = segue.destination as! DisclaimerViewController
            destinationVC.nameTyped = userName.text
        }
        if segue.identifier == "goToTask" {
            let destinationVC = segue.destination as! TaskViewController
            destinationVC.nameTyped = userName.text
        }
    }
    
}

