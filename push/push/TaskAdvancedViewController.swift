//
//  TaskAdvancedViewController.swift
//  push
//
//  Created by Cleber Cassiani on 22/2/19.
//  Copyright © 2019 Cleber Cassiani. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class TaskAdvancedViewController: UIViewController {
    
    var nameTyped : String?

    @IBOutlet weak var userName: UILabel!
    
    /// URL for the Exercise 1 of the PUSH program.
    let urlVideo1 : String = "https://player.vimeo.com/external/290411940.sd.mp4?s=98dbafca941d1a44a13188b5873765a3e20a09ed&profile_id=165"
    /// URL for the Exercise 2 of the PUSH program.
    let urlVideo2 : String = "https://player.vimeo.com/external/290411851.sd.mp4?s=6515e09865b1c1579fcede926ba10a29f40fbe9f&profile_id=165"
    /// URL for the Exercise 3 of the PUSH program.
    let urlVideo3 : String = "https://player.vimeo.com/external/290411890.sd.mp4?s=1103453dd7163c3d2588ceafa50074b7dfc55e90&profile_id=165"
    /// URL for the Exercise 4 of the PUSH program.
    let urlVideo4 : String = "https://player.vimeo.com/external/290411909.sd.mp4?s=96cb19436d1583a4f51d9dc351a0c48612838c1b&profile_id=165"
    /// URL for the Exercise 5 of the PUSH program.
    let urlVideo5 : String = "https://player.vimeo.com/external/290411864.sd.mp4?s=db8fa0bd7b6e984e06e6e1f30028574641d910a2&profile_id=165"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userName.text = nameTyped
        // Do any additional setup after loading the view.
    }
    
    func loadVideo(videoURL : String){
        guard let url = URL(string: videoURL) else {
            return
        }
        
        // Create an AVPlayer, passing it the HTTP Live Streaming URL.
        let player = AVPlayer(url: url)
        
        // Create a new AVPlayerViewController and pass it a reference to the player.
        let controller = AVPlayerViewController()
        controller.player = player
        
        // Modally present the player and call the player's play() method when complete.
        present(controller, animated: true) {
            player.play()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goHomeFromTaskAdvaced" {
            let destinationVC = segue.destination as! ViewController
            destinationVC.nameTyped = self.nameTyped
        }
        if segue.identifier == "goBackFromTaskAdvanced" {
            let destinationVC = segue.destination as! TaskViewController
            destinationVC.nameTyped = self.nameTyped
        }
        if segue.identifier == "goToTaskEmail2" {
            let destinationVC = segue.destination as! TaskEmailViewController
            destinationVC.nameTyped = self.nameTyped
        }
    }
    
    @IBAction func pressExercise1(_ sender: Any) {
        loadVideo(videoURL: urlVideo1)
    }
    
    @IBAction func pressExercise2(_ sender: Any) {
        loadVideo(videoURL: urlVideo2)
    }
    
    @IBAction func pressExercise3(_ sender: Any) {
        loadVideo(videoURL: urlVideo3)
    }
    
    @IBAction func pressExercise4(_ sender: Any) {
        loadVideo(videoURL: urlVideo4)
    }
    
    @IBAction func pressExercise5(_ sender: Any) {
        loadVideo(videoURL: urlVideo5)
    }
    
    @IBAction func pressHome(_ sender: Any) {
        performSegue(withIdentifier: "goHomeFromTaskAdvaced", sender: self)
    }
    
    @IBAction func pressReturn(_ sender: Any) {
        performSegue(withIdentifier: "goBackFromTaskAdvanced", sender: self)
    }
    
    @IBAction func pressSendMail(_ sender: Any) {
        performSegue(withIdentifier: "goToTaskEmail2", sender: self)
    }
    
}
