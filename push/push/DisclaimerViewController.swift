//
//  DisclaimerViewController.swift
//  push
//
//  Created by Cleber Cassiani on 6/11/18.
//  Copyright © 2018 Cleber Cassiani. All rights reserved.
//

import UIKit


/// This is the class responsible for displaying the Disclaimer for the PUSH program.
class DisclaimerViewController: UIViewController {

    /// Helper variable to hold the userName, initialized as "Type your name here" when no name is passed.
    var nameTyped : String? = "Type your name here"
    
    /// Loads the view
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    /// Takes the user to the Homepage, accepting the disclaimer.
    ///
    /// - Parameter sender: self
    @IBAction func pressAccept(_ sender: Any) {
        performSegue(withIdentifier: "acceptDisclaimer", sender: self)
    }
    
    /// This method passes data on to the next page. It uses the identifier to define which screen comes next, accordingly with user input.
    ///
    /// - Parameters:
    ///   - segue: the next page, defined when the user presses a button.
    ///   - sender: optional.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "acceptDisclaimer" {
            let destinationVC = segue.destination as! ViewController
            destinationVC.nameTyped = nameTyped
        }
    }
}
