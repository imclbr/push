//
//  SecondViewController.swift
//  push
//
//  Created by Cleber Cassiani on 1/10/18.
//  Copyright © 2018 Cleber Cassiani. All rights reserved.
//

import UIKit


/// This class displays the user the two methods of exercising within the PUSH program.
class PushViewController: UIViewController {
    
    /// Helper variable to recieve the name from another class
    var nameTyped : String?
    
    /// Label responsible to display the username in the view
    @IBOutlet weak var userName: UILabel!
    
    /// Loads the view
    override func viewDidLoad() {
        super.viewDidLoad()
        userName.text = nameTyped
    }
    
    /// From this page, navigage to "Sitting" exercises page.
    ///
    /// - Parameter sender: self
    @IBAction func sittingPressed(_ sender: Any) {
        performSegue(withIdentifier: "goToSitting", sender: self)
    }
    
    /// From this page, navigage to "Lying" exercises page.
    ///
    /// - Parameter sender: self
    @IBAction func standingPressed(_ sender: Any) {
        performSegue(withIdentifier: "goToLying", sender: self)
    }
    
    @IBAction func infoPressed(_ sender: Any) {
        performSegue(withIdentifier: "goToPushInfo", sender: self)
    }
    /// From this page, navigage to "Home" page.
    ///
    /// - Parameter sender: self
    @IBAction func homePressed(_ sender: Any) {
        performSegue(withIdentifier: "goToHomeFromPush", sender: self)
    }
    
    /// From this page, navigage to previous page.
    ///
    /// - Parameter sender: self
    @IBAction func returnPressed(_ sender: Any) {
        performSegue(withIdentifier: "goToHomeFromPush", sender: self)
    }
    
    /// This method passes data on to the next page. It uses the identifier to define which screen comes next, accordingly with user input
    ///
    /// - Parameters:
    ///   - segue: the next page, defined when the user presses a button.
    ///   - sender: optional.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToSitting" {
            let destinationVC = segue.destination as! PushSittingViewController
            destinationVC.nameTyped = self.nameTyped
        }
        if segue.identifier == "goToLying" {
            let destinationVC = segue.destination as! PushLyingViewController
            destinationVC.nameTyped = self.nameTyped
        }
        if segue.identifier == "goToHomeFromPush" {
            let destinationVC = segue.destination as! ViewController
            destinationVC.nameTyped = self.nameTyped
        }
        if segue.identifier == "goToPushInfo" {
            let destinationVC = segue.destination as! PushInfoViewController
            destinationVC.nameTyped = self.nameTyped
        }
    }

}
