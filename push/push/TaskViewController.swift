//
//  TaskViewController.swift
//  push
//
//  Created by Cleber Cassiani on 22/2/19.
//  Copyright © 2019 Cleber Cassiani. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class TaskViewController: UIViewController {
    
    var nameTyped : String? = "Name"

    @IBOutlet weak var userName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userName.text = nameTyped
    }
    
    @IBAction func pressTaskIntro(_ sender: Any) {
        guard let url = URL(string: "https://player.vimeo.com/external/290411835.sd.mp4?s=36c61887638f0608447e71a9ea3f3f66f58aca41&profile_id=165") else {
            return
        }
        
        // Create an AVPlayer, passing it the HTTP Live Streaming URL.
        let player = AVPlayer(url: url)
        
        // Create a new AVPlayerViewController and pass it a reference to the player.
        let controller = AVPlayerViewController()
        controller.player = player
        
        // Modally present the player and call the player's play() method when complete.
        present(controller, animated: true) {
            player.play()
        }
    }
    
    @IBAction func pressTaskBeginner(_ sender: Any) {
        performSegue(withIdentifier: "goToTaskBeginner", sender: self)
    }
    
    @IBAction func pressTaskAdvanced(_ sender: Any) {
        performSegue(withIdentifier: "goToTaskAdvanced", sender: self)
    }
    
    @IBAction func pressTaskInfo(_ sender: Any) {
        performSegue(withIdentifier: "goToTaskInfo", sender: self)
    }
    
    @IBAction func pressTaskEquipment(_ sender: Any) {
        if let url = URL(string: "https://www.mq.edu.au/about/about-the-university/faculties-and-departments/medicine-and-health-sciences/departments-and-centres/department-of-health-professions/our-research/task-program/equipment") {
            if UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    
    @IBAction func pressHome(_ sender: Any) {
        performSegue(withIdentifier: "goToHomeFromTask", sender: self)
    }
    
    @IBAction func pressReturn(_ sender: Any) {
        performSegue(withIdentifier: "goToHomeFromTask", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToTaskBeginner" {
            let destinationVC = segue.destination as! TaskBeginnerViewController
            destinationVC.nameTyped = nameTyped
        }
        if segue.identifier == "goToTaskAdvanced" {
            let destinationVC = segue.destination as! TaskAdvancedViewController
            destinationVC.nameTyped = nameTyped
        }
        if segue.identifier == "goToHomeFromTask" {
            let destinationVC = segue.destination as! ViewController
            destinationVC.nameTyped = nameTyped
        }
        if segue.identifier == "goToTaskInfo" {
            let destinationVC = segue.destination as! TaskInfoViewController
            destinationVC.nameTyped = nameTyped
        }
    }

}
