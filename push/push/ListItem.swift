//
//  listItem.swift
//  push
//
//  Created by Cleber Cassiani on 4/10/18.
//  Copyright © 2018 Cleber Cassiani. All rights reserved.
//

import Foundation

/// The purpose of this class is to create the template necessary for the data in the table view in *SendEmailViewController*.
class ListItem {
    
    //MARK: Variables
    
    ///The title of the exercise
    var title : String
    ///The state of the exercise, selected or not selected
    var selected : Bool
    
    public init (title : String){
        self.title = title
        self.selected = false
    }
    
}

// MARK: - This extension easily generates the data for the PUSH Program.
extension ListItem
{
    
    /// Generate the list of exercises for the PUSH program
    ///
    /// - Returns: a list containing the titles of all exercises from the PUSH program.
    public class func getData() -> [ListItem]
    {
        return [
            ListItem(title: "Exercise 1 - The Shoulder PUSH"),
            ListItem(title: "Exercise 2 - The Arm PUSH"),
            ListItem(title: "Exercise 3 - The Elbow Bend & Straighten"),
            ListItem(title: "Exercise 4 - Visualise Hand Movements"),
            ListItem(title: "Exercise 5 - The Thumb Slide"),
            ListItem(title: "Exercise 6 - Hold Onto Your Head"),
            ListItem(title: "Exercise 7 - The In & Out")
        ]
    }
    
    /// Generate the list of exercises for the TASK program
    ///
    /// - Returns: a list containing the titles of all exercises from the PUSH program.
    public class func getTaskData() -> [ListItem]
    {
        return [
            ListItem(title: "TASK Begginner: Sit and Reach"),
            ListItem(title: "TASK Begginner: Stand and Reach"),
            ListItem(title: "TASK Begginner: Step Tap"),
            ListItem(title: "TASK Begginner: Stepping"),
            ListItem(title: "TASK Begginner: Sit to Stand"),
            ListItem(title: "TASK Advanced: Sit and Reach"),
            ListItem(title: "TASK Advanced: Stand and Reach"),
            ListItem(title: "TASK Advanced: Step Tap"),
            ListItem(title: "TASK Advanced: Stepping"),
            ListItem(title: "TASK Advanced: Sit to Stand")
        ]
    }
}
