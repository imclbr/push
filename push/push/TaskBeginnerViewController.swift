//
//  TaskBeginnerViewController.swift
//  push
//
//  Created by Cleber Cassiani on 22/2/19.
//  Copyright © 2019 Cleber Cassiani. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class TaskBeginnerViewController: UIViewController {
    
    var nameTyped : String?
    
    @IBOutlet weak var userName: UILabel!
    
    /// URL for the Exercise 1 of the TASK Beginner program.
    let urlVideo1 : String = "https://player.vimeo.com/external/290411815.sd.mp4?s=4da8ab91430fc0fde9392cd2f158c8d6c7ac8d7a&profile_id=165"
    /// URL for the Exercise 2 of the TASK Beginner program.
    let urlVideo2 : String = "https://player.vimeo.com/external/290411789.sd.mp4?s=fd2cf2a528f2f2b593e8387740c2310e7d8cfeb0&profile_id=165"
    /// URL for the Exercise 3 of the TASK Beginner program.
    let urlVideo3 : String = "https://player.vimeo.com/external/290411804.sd.mp4?s=5664c327aca7e5c18d6efb224acb8827aa6500dd&profile_id=165"
    /// URL for the Exercise 4 of the TASK Beginner program.
    let urlVideo4 : String = "https://player.vimeo.com/external/290411927.sd.mp4?s=c448e1c05d7d8e1cd47b41800a020f739386a519&profile_id=165"
    /// URL for the Exercise 5 of the TASK Beginner program.
    let urlVideo5 : String = "https://player.vimeo.com/external/290411880.sd.mp4?s=dbe427e9ddd0d83accf38a18f3d6591f1bee05a3&profile_id=165"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userName.text = nameTyped
        // Do any additional setup after loading the view.
    }
    
    func loadVideo(videoURL : String){
        guard let url = URL(string: videoURL) else {
            return
        }
        
        // Create an AVPlayer, passing it the HTTP Live Streaming URL.
        let player = AVPlayer(url: url)
        
        // Create a new AVPlayerViewController and pass it a reference to the player.
        let controller = AVPlayerViewController()
        controller.player = player
        
        // Modally present the player and call the player's play() method when complete.
        present(controller, animated: true) {
            player.play()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goHomeFromTaskBeginner" {
            let destinationVC = segue.destination as! ViewController
            destinationVC.nameTyped = self.nameTyped
        }
        if segue.identifier == "goBackFromTaskBeginner" {
            let destinationVC = segue.destination as! TaskViewController
            destinationVC.nameTyped = self.nameTyped
        }
        if segue.identifier == "goToTaskEmail" {
            let destinationVC = segue.destination as! TaskEmailViewController
            destinationVC.nameTyped = self.nameTyped
        }
    }
    
    @IBAction func pressExercise1(_ sender: Any) {
        loadVideo(videoURL: urlVideo1)
    }
    
    @IBAction func pressExercise2(_ sender: Any) {
        loadVideo(videoURL: urlVideo2)
    }
    
    @IBAction func pressExercise3(_ sender: Any) {
        loadVideo(videoURL: urlVideo3)
    }
    
    @IBAction func pressExercise4(_ sender: Any) {
        loadVideo(videoURL: urlVideo4)
    }
    
    @IBAction func pressExercise5(_ sender: Any) {
        loadVideo(videoURL: urlVideo5)
    }
    
    @IBAction func pressSendMail(_ sender: Any) {
        performSegue(withIdentifier: "goToTaskEmail", sender: self)
    }
    
    @IBAction func pressHome(_ sender: Any) {
        performSegue(withIdentifier: "goHomeFromTaskBeginner", sender: self)
    }
    
    @IBAction func pressReturn(_ sender: Any) {
        performSegue(withIdentifier: "goBackFromTaskBeginner", sender: self)
        
    }
    
}
