//
//  InfoViewController.swift
//  push
//
//  Created by Cleber Cassiani on 22/10/18.
//  Copyright © 2018 Cleber Cassiani. All rights reserved.
//

import UIKit


/// This class is responsible to display the information about the PUSH program.
class InfoViewController: UIViewController {
    
    /// Helper variable to hold the user name.
    var nameTyped : String?

    
    /// Loads the view
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    /// Takes the user to the Homepage.
    ///
    /// - Parameter sender: self.
    @IBAction func pressHome(_ sender: Any) {
        performSegue(withIdentifier: "goHomeFromInfo", sender: self)
    }
    
    /// This method helps to pass on data to the next screen. It is used to display the user name on the next screens and help with the email class.
    ///
    /// - Parameters:
    ///   - segue: the next view
    ///   - sender: self
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goHomeFromInfo" {
            let destinationVC = segue.destination as! ViewController
            destinationVC.nameTyped = nameTyped
        }
    }

}
