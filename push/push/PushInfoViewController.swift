//
//  PushInfoViewController.swift
//  push
//
//  Created by Cleber Cassiani on 24/3/19.
//  Copyright © 2019 Cleber Cassiani. All rights reserved.
//

import UIKit

class PushInfoViewController: UIViewController {
    
    var nameTyped : String?

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func pressHome(_ sender: Any) {
        performSegue(withIdentifier: "goHomeFromPushInfo", sender: self)
    }
    
    @IBAction func pressReturn(_ sender: Any) {
        performSegue(withIdentifier: "goBackFromPushInfo", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goHomeFromPushInfo" {
            let destinationVC = segue.destination as! ViewController
            destinationVC.nameTyped = nameTyped
        }
        if segue.identifier == "goBackFromPushInfo" {
            let destinationVC = segue.destination as! PushViewController
            destinationVC.nameTyped = nameTyped
        }
    }
    
}
