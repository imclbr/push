//
//  TaskInfoViewController.swift
//  push
//
//  Created by Cleber Cassiani on 24/3/19.
//  Copyright © 2019 Cleber Cassiani. All rights reserved.
//

import UIKit

class TaskInfoViewController: UIViewController {
    
    var nameTyped : String?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func pressHome(_ sender: Any) {
        performSegue(withIdentifier: "goHomeFromTaskInfo", sender: self)

    }
    
    
    @IBAction func pressReturn(_ sender: Any) {
        performSegue(withIdentifier: "goBackFromTaskInfo", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goHomeFromTaskInfo" {
            let destinationVC = segue.destination as! ViewController
            destinationVC.nameTyped = nameTyped
        }
        if segue.identifier == "goBackFromTaskInfo" {
            let destinationVC = segue.destination as! TaskViewController
            destinationVC.nameTyped = nameTyped
        }
    }
    
}
