//
//  PushSittingViewController.swift
//  push
//
//  Created by Cleber Cassiani on 1/10/18.
//  Copyright © 2018 Cleber Cassiani. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit


/// This is the class responsible for displaying the sitting videos for the PUSH program.
class PushSittingViewController: UIViewController {

    /// Helper variable to hold the user name.
    var nameTyped : String?
    
    /// Label that displays the user name.
    @IBOutlet weak var userName: UILabel!
    
    /// URL for the Exercise 1 of the PUSH program.
    let urlVideo1 : String = "https://player.vimeo.com/external/285448922.sd.mp4?s=c03c44e54588ec64fb98e1d227242e4ade952e88&profile_id=165"
    /// URL for the Exercise 2 of the PUSH program.
    let urlVideo2 : String = "https://player.vimeo.com/external/285448955.sd.mp4?s=1c2a3ed42e208ffb6e8c851609d1515c50181412&profile_id=165"
    /// URL for the Exercise 3 of the PUSH program.
    let urlVideo3 : String = "https://player.vimeo.com/external/285448991.sd.mp4?s=84755f5d0c7bfe9dd5e56cb9dc0f641540d3750e&profile_id=165"
    /// URL for the Exercise 4 of the PUSH program.
    let urlVideo4 : String = "https://player.vimeo.com/external/285449022.sd.mp4?s=c26a8d9473f0eaccf5444c33e3ebd26ea1588c51&profile_id=165"
    /// URL for the Exercise 5 of the PUSH program.
    let urlVideo5 : String = "https://player.vimeo.com/external/285449064.sd.mp4?s=a9b3981d4b6a190d82af0ca03540432915159779&profile_id=165"

    /// Loads the view
    override func viewDidLoad() {
        super.viewDidLoad()
        userName.text = nameTyped
    }
    
    /// Player button for the Exercise 1.
    @IBAction func pressExercise1(_ sender: Any) {
        loadVideo(videoURL: urlVideo1)
    }
    
    /// Player button for the Exercise 1.
    @IBAction func pressExercise2(_ sender: Any) {
        loadVideo(videoURL: urlVideo2)
    }
    
    /// Player button for the Exercise 1.
    @IBAction func pressExercise3(_ sender: Any) {
        loadVideo(videoURL: urlVideo3)
    }
    
    /// Player button for the Exercise 1.
    @IBAction func pressExercise4(_ sender: Any) {
        loadVideo(videoURL: urlVideo4)
    }
    
    /// Player button for the Exercise 1.
    @IBAction func pressExercise5(_ sender: Any) {
        loadVideo(videoURL: urlVideo5)
    }
    
    /// This method is responsible for loading the video and oppening the iOS video player. It was built as recommended on Apple's website.
    ///
    /// - Parameter videoURL: URL of the video to be played.
    func loadVideo(videoURL : String){
        guard let url = URL(string: videoURL) else {
            return
        }
        
        // Create an AVPlayer, passing it the HTTP Live Streaming URL.
        let player = AVPlayer(url: url)
        
        // Create a new AVPlayerViewController and pass it a reference to the player.
        let controller = AVPlayerViewController()
        controller.player = player
        
        // Modally present the player and call the player's play() method when complete.
        present(controller, animated: true) {
            player.play()
        }
    }
    
    /// Takes the user to the send email page, to select the exercises done and email the person desired.
    ///
    /// - Parameter sender: self
    @IBAction func sendEmail(_ sender: Any) {
        performSegue(withIdentifier: "goToSendMail", sender: self)
    }
    
    /// Takes the user back to the Homepage.
    ///
    /// - Parameter sender: self
    @IBAction func pressHome(_ sender: Any) {
        performSegue(withIdentifier: "homeFromPushSitting", sender: self)
    }
    
    /// Takes the user back to the previous page.
    ///
    /// - Parameter sender: self
    @IBAction func pressReturn(_ sender: Any) {
        performSegue(withIdentifier: "returnFromPushSitting", sender: self)
    }
    
    /// This method passes data on to the next page. It uses the identifier to define which screen comes next, accordingly with user input.
    ///
    /// - Parameters:
    ///   - segue: the next page, defined when the user presses a button.
    ///   - sender: optional.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "homeFromPushSitting" {
            let destinationVC = segue.destination as! ViewController
            destinationVC.nameTyped = self.nameTyped
        }
        if segue.identifier == "returnFromPushSitting" {
            let destinationVC = segue.destination as! PushViewController
            destinationVC.nameTyped = self.nameTyped
        }
        if segue.identifier == "goToSendMail" {
            let destinationVC = segue.destination as! SendEmailViewController
            destinationVC.nameTyped = self.nameTyped
        }
    }

}
