//
//  PushStandingViewController.swift
//  push
//
//  Created by Cleber Cassiani on 1/10/18.
//  Copyright © 2018 Cleber Cassiani. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit


/// This is the class responsible for displaying the lying videos for the PUSH program.
class PushLyingViewController: UIViewController {
    
    /// Helper variable to hold the user name.
    var nameTyped : String?
    
    /// Label that displays the user name.
    @IBOutlet weak var userName: UILabel!
    
    /// URL for the Exercise 6 of the PUSH program.
    let urlVideo6 : String = "https://player.vimeo.com/external/285449098.sd.mp4?s=35edb46b887b6e906ace6baa67969eeefcc778df&profile_id=165"
    /// URL for the Exercise 7 of the PUSH program.
    let urlVideo7 : String = "https://player.vimeo.com/external/285449147.sd.mp4?s=856d6598fd641511852d9c4a9265ae16408eb785&profile_id=165"

    /// Loads the view
    override func viewDidLoad() {
        super.viewDidLoad()
        userName.text = nameTyped
    }
    
    /// Takes the user to the send email page, to select the exercises done and email the person desired.
    ///
    /// - Parameter sender: self
    @IBAction func sendEmail(_ sender: Any) {
        performSegue(withIdentifier: "goToSendMail2", sender: self)
    }
    
    /// Player button for the Exercise 6.
    @IBAction func pressExercise6(_ sender: Any) {
        loadVideo(videoURL: urlVideo6)
    }
    
    /// Player button for the Exercise 7.
    @IBAction func pressExercise7(_ sender: Any) {
        loadVideo(videoURL: urlVideo7)
    }
    
    /// This method is responsible for loading the video and oppening the iOS video player. It was built as recommended on Apple's website.
    ///
    /// - Parameter videoURL: URL of the video to be played.
    func loadVideo(videoURL : String){
        guard let url = URL(string: videoURL) else {
            return
        }
        
        // Create an AVPlayer, passing it the HTTP Live Streaming URL.
        let player = AVPlayer(url: url)
        
        // Create a new AVPlayerViewController and pass it a reference to the player.
        let controller = AVPlayerViewController()
        controller.player = player
        
        // Modally present the player and call the player's play() method when complete.
        present(controller, animated: true) {
            player.play()
        }
    }
    
    /// Takes the user back to the Homepage.
    ///
    /// - Parameter sender: self
    @IBAction func pressHome(_ sender: Any) {
        performSegue(withIdentifier: "homeFromPushLying", sender: self)
    }
    
    /// Takes the user back to the previous page.
    ///
    /// - Parameter sender: self
    @IBAction func pressReturn(_ sender: Any) {
        performSegue(withIdentifier: "returnFromPushLying", sender: self)
    }
    
    /// This method passes data on to the next page. It uses the identifier to define which screen comes next, accordingly with user input.
    ///
    /// - Parameters:
    ///   - segue: the next page, defined when the user presses a button.
    ///   - sender: optional.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "homeFromPushLying" {
            let destinationVC = segue.destination as! ViewController
            destinationVC.nameTyped = self.nameTyped
        }
        if segue.identifier == "returnFromPushLying" {
            let destinationVC = segue.destination as! PushViewController
            destinationVC.nameTyped = self.nameTyped
        }
        if segue.identifier == "goToSendMail2" {
            let destinationVC = segue.destination as! SendEmailViewController
            destinationVC.nameTyped = self.nameTyped
        }
    }
}
