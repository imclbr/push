//
//  SendEmailViewController.swift
//  push
//
//  Created by Cleber Cassiani on 3/10/18.
//  Copyright © 2018 Cleber Cassiani. All rights reserved.
//

import UIKit
import MessageUI


/// This class is responsible for handling the exercises selection and the trigger to open email, prefilled with subject and body.
class SendEmailViewController: UIViewController, MFMailComposeViewControllerDelegate, UITableViewDelegate, UITableViewDataSource {
    
    /// Table responsible to hold the exercises names.
    @IBOutlet weak var pushTableView: UITableView!
    
    /// Label to display the user name.
    @IBOutlet weak var userName: UILabel!
    
    /// List of the exercise names.
    private var exerciseNames = ListItem.getData()
    
    /// Helper variable to recieve and pass on the user name.
    var nameTyped : String?
    
    /// Loads the view
    override func viewDidLoad() {
        super.viewDidLoad()
        userName.text = nameTyped
    }
    
    /// Table view which will load and display the exercise names.
    ///
    /// - Parameters:
    ///   - tableView: the table itself
    ///   - section: row
    /// - Returns: number of rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return exerciseNames.count
    }
    
    /// Table view which will load and display the exercise names.
    ///
    /// - Parameters:
    ///   - tableView: the table itself.
    ///   - indexPath: formatting of the cells.
    /// - Returns: returns the formatted table.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        if indexPath.row < exerciseNames.count
        {
            let item = exerciseNames[indexPath.row]
            cell.textLabel?.text = item.title
            
            let accessory: UITableViewCell.AccessoryType = item.selected ? .checkmark : .none
            cell.accessoryType = accessory
        }
        
        return cell
    }
    
    /// Table view which will load and display the exercise names. This part is responsible to display which items are selected.
    ///
    /// - Parameters:
    ///   - tableView: the table view itself.
    ///   - indexPath: selected items.
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.row < exerciseNames.count
        {
            let item = exerciseNames[indexPath.row]
            item.selected = !item.selected
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }
    }

    
    /// Button that will open the iOS email application.
    ///
    /// - Parameter sender: self
    @IBAction func sendEmail(_ sender: Any) {
        let mailComposerViewController = configureMailController()
        if MFMailComposeViewController.canSendMail(){
            self.present(mailComposerViewController, animated: true, completion: nil)
        }else {
            showMailError()
        }
    }
    
    /// This method configures the email subject and body.
    ///
    /// - Returns: the email formatted.
    func configureMailController() -> MFMailComposeViewController {
        let mailVC = MFMailComposeViewController()
        
        mailVC.mailComposeDelegate = self
        mailVC.setToRecipients([""])
        mailVC.setSubject("PUSH App Usage")
        mailVC.setMessageBody(elaborateEmailBody(), isHTML: false)
        
        return mailVC
    }
    
    
    /// This method formats the body of the email with the user selection from the tableview
    ///
    /// - Returns: email body in text format
    func elaborateEmailBody() -> String{
        
        var body : String = ""
        var exercisesDone: [String] = []
        
        for item in exerciseNames {
            if item.selected {
                exercisesDone.append(item.title)
            }
        }
        
        body.append(self.nameTyped!)
        body.append(" has completed the following exercises: \n \n")
        
        for item in exerciseNames {
            if item.selected {
                body.append("[X]" + item.title + "\n")
            }
        }
        
        body.append("\n \n \n Feedback:")
        
        return body
    }
    
    /// Handles any error.
    func showMailError() {
        let mailErrorAlert = UIAlertController(title: "Could not send email", message: "Not sent", preferredStyle: .alert)
        let dismiss = UIAlertAction(title: "Ok", style: .default, handler: nil)
        mailErrorAlert.addAction(dismiss)
        self.present(mailErrorAlert, animated: true, completion: nil)
    }
    
    /// Send alert in case of error.
    ///
    /// - Parameters:
    ///   - controller: Mail view controller.
    ///   - result: either success or fail.
    ///   - error: discription of the error.
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    /// Takes the user back to the home screen
    ///
    /// - Parameter sender: self.
    @IBAction func pressHome(_ sender: Any) {
        performSegue(withIdentifier: "goHomeFromMail", sender: self)
    }
    
    /// This method helps to pass on data to the next screen. It is used to display the user name on the next screens and help with the email class.
    ///
    /// - Parameters:
    ///   - segue: the next view
    ///   - sender: self
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goHomeFromMail" {
            let destinationVC = segue.destination as! ViewController
            destinationVC.nameTyped = self.nameTyped
        }
    }
}
